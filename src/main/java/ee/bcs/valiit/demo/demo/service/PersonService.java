package ee.bcs.valiit.demo.demo.service;

import ee.bcs.valiit.demo.demo.model.Person;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.validation.constraints.Null;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class PersonService {

    List<Person> personsList = new ArrayList<>();

    @PostConstruct
    public void init() {
        Person firstP = new Person();
        firstP.setFirstName("Katre");
        firstP.setLastName("Juganson");
        firstP.setSocialSecurityId(48806090310L);

        Person secondP = new Person();
        secondP.setFirstName("Miki");
        secondP.setLastName("Hiir");
        secondP.setSocialSecurityId(32811180000L);

        Person thirdP = new Person();
        thirdP.setFirstName("Donald");
        thirdP.setLastName("Duck");
        thirdP.setSocialSecurityId(33408110000L);

        Person fourthP = new Person();
        fourthP.setFirstName("Miki");
        fourthP.setLastName("Rott");
        fourthP.setSocialSecurityId(51808290000L);

        personsList.add(firstP);
        personsList.add(secondP);
        personsList.add(thirdP);
        personsList.add(fourthP);
        log.info("init done");
    }

    public List<Person> getPerson(String input) {

        List<Person> returnList = new ArrayList<>();

        for (int i = 0; i < personsList.size(); i++) {
            if (Long.toString(personsList.get(i).getSocialSecurityId()).equals(input) || personsList.get(i).getFirstName().equals(input) || personsList.get(i).getFirstName().toLowerCase().equals(input)) {
                returnList.add(personsList.get(i));
            }
        }
        if (returnList.isEmpty()) {
            Person person0 = new Person();
            person0.setFirstName(null);
            person0.setLastName(null);
            person0.setSocialSecurityId(0L);
            returnList.add(person0);
            return returnList;
        }
        return returnList;
    }


    public void changeName(String socialSecurityId, Person person) {
        Person changePerson = null;
        for (Person p : personsList) {
            if (p.getSocialSecurityId() == Long.parseLong(socialSecurityId)) {
                changePerson = p;
            }
        }
        personsList.set(personsList.indexOf(changePerson), person);
    }


    public void addPerson(Person person) {
        personsList.add(person);
    }

    public List<Person> list() {
        return personsList;
    }

}



