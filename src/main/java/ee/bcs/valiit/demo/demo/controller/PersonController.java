package ee.bcs.valiit.demo.demo.controller;


import ee.bcs.valiit.demo.demo.model.Person;
import ee.bcs.valiit.demo.demo.service.PersonService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@Slf4j
@RestController
public class PersonController {

    @Autowired
    PersonService personService;

    @RequestMapping(value = "/person", method = RequestMethod.GET)
    public List<Person> list() {
        log.debug("kas kood jõuab siia");

        return personService.list();

    }

    @RequestMapping(value = {"/person/{socialSecurityId}", "/person/{firstName}"}, method = RequestMethod.GET)
    public List<Person> getPerson(@PathVariable Optional<String> socialSecurityId,
                                  @PathVariable Optional<String> firstName) {
        log.debug("kas kood jõuab siia");

        if (socialSecurityId.isPresent()) {
            return personService.getPerson(socialSecurityId.get());
        } else {
            return personService.getPerson(firstName.get());

        }
    }

    @RequestMapping(value = "/person/{socialSecurityId}", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public void changeName(@PathVariable String socialSecurityId,
                           @RequestBody Person person) {
        personService.changeName(socialSecurityId, person);
    }


    @RequestMapping(value = "/person", method = RequestMethod.PUT, consumes = "application/json", produces = "application/json")
    public void addPerson(@RequestBody Person person) {
        personService.addPerson(person);
    }

}
